FROM node:latest
ENV APP_ROOT /

WORKDIR ${APP_ROOT}
ADD . ${APP_ROOT}

COPY package.json ${APP_ROOT}

COPY /dist ${APP_ROOT}

EXPOSE 4000

CMD ["npm", "run", "start:prod"]