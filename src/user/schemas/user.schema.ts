import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;
import { roleEnum } from '../enums/role.enum';
import { statusEnum } from '../enums/status.enum';

export const UserSchema = new mongoose.Schema({
    email: { type: String, lowercase: true, required: true },
    status: { type: String, enum: Object.values(statusEnum), default: statusEnum.pending },
    avatar: { type: String, default: null },
    avatarId: { type: String, default: null },
    firstname: { type: String, required: true },
    lastName: { type: String, required: true },
    roles: { type: [String], required: true, enum: Object.values(roleEnum) },
    password: { type: String, required: true },
    categories: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
});

UserSchema.index({ email: 1 }, { unique: true });
