import { IsEmail, IsString, IsNotEmpty, Matches } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
    @ApiProperty()
    @IsEmail()
    readonly email: string;

    readonly avatar: string;
    readonly avatarId: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    readonly firstname: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    readonly lastName: string;

    // readonly searchField: string;

    readonly roles: string[];

    @IsString()
    @IsNotEmpty()
    @Matches(
        /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/,
        { message: 'Некорректный пароль' },
    )
    @ApiProperty()
    readonly password: string;
}
