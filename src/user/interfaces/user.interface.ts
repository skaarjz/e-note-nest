import { Document } from 'mongoose';
// import { IAddress } from './address.interface';

export interface IUser extends Document {
    readonly email: string;
    status: string;
    readonly avatar: string;
    readonly avatarId: string;
    readonly firstname: string;
    readonly lastname: string;
    // readonly searchField: string;
    readonly roles: string[];
    readonly password: string;
}
