import {Controller, Post, Body, ValidationPipe, Get, Query, Request, UseGuards} from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import {ApiBearerAuth, ApiTags} from '@nestjs/swagger';
import { ConfirmAccountDto } from './dto/confirm-account.dto';
import { ChangePasswordDto } from './dto/change-password.dto';
import { ResetUserPasswordDto } from './dto/reset-password.dto';
import { SignInDto } from './dto/signin.dto';
import { IReadableUser } from 'src/user/interfaces/readable-user.interface';
import {getTokenFromHeader} from '../utils/getTokenFromHeader';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post('/signUp')
    async signUp(@Body(new ValidationPipe()) createUserDto: CreateUserDto): Promise<boolean> {
        return this.authService.signUp(createUserDto);
    }

    @Get('/confirm')
    async confirm(@Query(new ValidationPipe()) query: ConfirmAccountDto): Promise<boolean> {
        await this.authService.confirm(query.token);
        return true;
    }

    @Post('/reset')
    async resetPassword(@Body(new ValidationPipe()) resetUserPasswordDto: ResetUserPasswordDto): Promise<boolean> {
        await this.authService.sendResetPassword(resetUserPasswordDto);
        return true;
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt')
    @Post('/set-new-password')
    async setNewPassword(@Body(new ValidationPipe()) confirmAccountDto: ChangePasswordDto, @Request() req): Promise<boolean> {
        await this.authService.changePassword(confirmAccountDto, req);
        return true;
    }

    @Post('/signIn')
    async signIn(@Body(new ValidationPipe()) signInDto: SignInDto): Promise<IReadableUser> {
        return await this.authService.signIn(signInDto);
    }
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt')
    @Get('/me')
    async getUserByToken(@Request() req): Promise<object> {
        const token = getTokenFromHeader(req);
        return await this.authService.verifyUser(token);
    }
}
