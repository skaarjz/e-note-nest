import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetUserPasswordDto {
  @IsNotEmpty()
  @ApiProperty()
  email: string;
}
