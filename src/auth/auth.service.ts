import { Injectable, UnauthorizedException, BadRequestException, MethodNotAllowedException, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import * as _ from 'lodash';
import * as moment from 'moment';

import { UserService } from 'src/user/user.service';
import { TokenService } from 'src/token/token.service';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { SignOptions } from 'jsonwebtoken';
import { CreateUserTokenDto } from 'src/token/dto/create-user-token.dto';
import { roleEnum } from 'src/user/enums/role.enum';
import { IUser } from 'src/user/interfaces/user.interface';
import { ConfigService } from '@nestjs/config';
import { MailService } from 'src/mail/mail.service';
import { statusEnum } from 'src/user/enums/status.enum';
import { SignInDto } from './dto/signin.dto';
import { ITokenPayload } from './interfaces/token-payload.interface';
import { IReadableUser } from 'src/user/interfaces/readable-user.interface';
import { ChangePasswordDto } from './dto/change-password.dto';
import { ResetUserPasswordDto } from './dto/reset-password.dto';
import { userSensitiveFieldsEnum } from 'src/user/enums/protected-fields.enum';
import {getTokenFromHeader} from '../utils/getTokenFromHeader';

@Injectable()
export class AuthService {
    private readonly clientAppUrl: string;

    constructor(
        private readonly jwtService: JwtService,
        private readonly userService: UserService,
        private readonly tokenService: TokenService,
        private readonly configService: ConfigService,
        private readonly mailService: MailService,
    ) {
        this.clientAppUrl = this.configService.get<string>('FE_APP_URL');
    }

    async signUp(createUserDto: CreateUserDto): Promise<boolean> {
        const isEmailExist = !!await this.userService.findByEmail(createUserDto.email);
        if (isEmailExist) { throw new BadRequestException('Данный email уже используется'); } else {
            const user = await this.userService.create(createUserDto, [roleEnum.user]);
            await this.sendConfirmation(user);
            return true;
        }
    }

    async signIn({ email, password }: SignInDto): Promise<IReadableUser> {
        const user = await this.userService.findByEmail(email.toLowerCase());
        if (user && (await bcrypt.compare(password, user.password))) {
            if (user.status !== statusEnum.active) {
                throw new MethodNotAllowedException();
            }
            const tokenPayload: ITokenPayload = {
                _id: user._id,
                email: user.email,
            };
            const token = await this.generateToken(tokenPayload);
            const expireAt = moment()
                .add(1, 'day')
                .toISOString();

            await this.saveToken({
                token,
                expireAt,
                uId: user._id,
            });

            // const readableUser = { ...user.toObject() as IReadableUser, accessToken: token};
            return { accessToken: token, expireAt} as {accessToken: string, expireAt: string};

            // return _.omit<any>(readableUser, Object.values(userSensitiveFieldsEnum)) as IReadableUser;
        }
        throw new BadRequestException('Неправильный логин или пароль');
    }

    async changePassword(changePasswordDto: ChangePasswordDto, req): Promise<boolean> {
        const token = getTokenFromHeader(req);
        const user = await this.verifyToken(token);
        const password = await this.userService.hashPassword(changePasswordDto.password);
        await this.userService.update(user._id, { password });
        await this.tokenService.deleteAll(user._id);
        return true;
    }

    async confirm(token: string): Promise<IUser> {
        const data = await this.verifyToken(token);
        const user = await this.userService.find(data._id);

        await this.tokenService.delete(data._id, token);

        if (user && user.status === statusEnum.pending) {
            user.status = statusEnum.active;
            return user.save();
        }
        throw new BadRequestException('Произошла ошибка');
    }

    async sendConfirmation(user: IUser) {
        const expiresIn = 60 * 60 * 24; // 24 hours
        const tokenPayload = {
            _id: user._id,
            email: user.email,
        };
        const expireAt = moment()
            .add(1, 'day')
            .toISOString();

        const token = await this.generateToken(tokenPayload, { expiresIn });
        const confirmLink = `${this.clientAppUrl}/auth/confirm?token=${token}`;

        await this.saveToken({ token, uId: user._id, expireAt });
        await this.mailService.send({
            from: this.configService.get<string>('JS_CODE_MAIL'),
            to: user.email,
            subject: 'Подтверждение регистрации',
            html: `
                <h3>Привет ${user.firstname}!</h3>
                <p>Перейди по этой
                    <a href="${confirmLink}">ссылке</a> чтобы подтвердить регистрацию на сайте
                    <a href="https://e-note.online">e-note.online</a>.
                </p>
            `,
        });
    }

    async sendResetPassword(updatedUser: ResetUserPasswordDto) {
        const user = await this.userService.findByEmail(updatedUser.email);
        if (!user) { throw new NotFoundException('Пользователь с таким email не найден'); }

        const expiresIn = 60 * 60; // 1 hour

        const expireAt = moment()
            .add(1, 'hour')
            .toISOString();

        const tokenPayload = {
            _id: user._id,
            email: updatedUser.email,
        };

        const token = await this.generateToken(tokenPayload, { expiresIn });
        const confirmLink = `${this.clientAppUrl}/auth/reset?token=${token}`;

        await this.saveToken({ token, uId: user._id, expireAt });
        await this.mailService.send({
            from: this.configService.get<string>('JS_CODE_MAIL'),
            to: user.email,
            subject: 'Восстановление пароля',
            html: `
                <h3>Привет ${user.firstname}!</h3>
                <p>Перейди по этой
                    <a href="${confirmLink}">ссылке</a> чтобы чтобы восстановить свой пароль на сайте
                    <a href="https://e-note.online">e-note.online</a>.
                </p>
            `,
        });
    }

    private async generateToken(data: ITokenPayload, options?: SignOptions): Promise<string> {
        return this.jwtService.sign(data, options);
    }

    async verifyToken(token): Promise<any> {
        try {
            const data = this.jwtService.verify(token) as ITokenPayload;
            const tokenExists = await this.tokenService.exists(data._id, token);

            if (tokenExists) {
                return data;
            }
            throw new UnauthorizedException();
        } catch (error) {
            throw new UnauthorizedException();
        }
    }

    async verifyUser(token): Promise<any> {
        try {
            const data = this.jwtService.verify(token) as ITokenPayload;
            const user = await this.userService.find(data._id);
            const tokenExists = await this.tokenService.exists(data._id, token);

            if (tokenExists) {
                const readableUser = user.toObject() as IReadableUser;
                return _.omit<any>(readableUser, Object.values(userSensitiveFieldsEnum)) as IReadableUser;
            }
            throw new UnauthorizedException();
        }   catch (error) {
            throw new UnauthorizedException();
        }
    }

    private async saveToken(createUserTokenDto: CreateUserTokenDto) {
        return await this.tokenService.create(createUserTokenDto);
    }
}
