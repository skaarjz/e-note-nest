import { Module } from '@nestjs/common';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CategorySchema } from './schemas/category.schema';
import {AuthModule} from '../auth/auth.module';
import {AuthService} from '../auth/auth.service';
import {JwtModule} from '@nestjs/jwt';
import {UserModule} from '../user/user.module';
import {TokenModule} from '../token/token.module';
import {MailModule} from '../mail/mail.module';

@Module({
  imports: [
      AuthModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1d' },
    }),
      UserModule,
      TokenModule,
      MailModule,
    MongooseModule.forFeature([{ name: 'Category', schema: CategorySchema }]),
  ],
  controllers: [CategoryController],
  providers: [CategoryService, AuthService],
})
export class CategoryModule { }
