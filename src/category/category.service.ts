import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Category } from './interfaces/category.interface';
import { ITokenPayload } from '../auth/interfaces/token-payload.interface';
import { CreateCategoryDto } from './dto/create-category.dto';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class CategoryService {
    constructor(
        @InjectModel('Category') private readonly categoryModel: Model<Category>,
        private readonly authModel: AuthService,
    ) { }

    async getCategories(): Promise<Category[]> {
        return await this.categoryModel.find().exec();
    }

    async getCategoryOwner(req): Promise<Category> {
        return await this.categoryModel.findOne({ name: 'Охуеть' }).
        populate('user', 'firstname').
        exec();
    }

    async createCategory(createCategoryDto: CreateCategoryDto, token): Promise<Category> {
        const user = await this.authModel.verifyToken(token);
        createCategoryDto.user = user._id;
        const category = await new this.categoryModel(createCategoryDto);
        return await category.save();
    }
}
