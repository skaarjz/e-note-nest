import * as mongoose from 'mongoose';

const {ObjectId} = mongoose.Schema.Types;

export const CategorySchema = new mongoose.Schema({
    name: { type: String, lowercase: true, required: true },
    services: [{ type: ObjectId, ref: 'Service' }],
    user: { type: ObjectId, ref: 'User' },
});
