import {
    Controller,
    Get,
    Res,
    HttpStatus,
    Param,
    NotFoundException,
    Post,
    Body,
    Query,
    Put,
    Delete,
    Request,
    UseGuards,
} from '@nestjs/common';
import { CategoryService } from './category.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import {getTokenFromHeader} from '../utils/getTokenFromHeader';
import {ApiBearerAuth, ApiTags} from '@nestjs/swagger';
import {JwtAuthGuard} from '../auth/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth('jwt')
@ApiTags('categories')
@Controller('categories')
export class CategoryController {

    constructor(private categoryService: CategoryService) { }

    @Get('list')
    async getCategoriesList(@Res() res) {
        const posts = await this.categoryService.getCategories();
        return res.status(HttpStatus.OK).json(posts);
    }

    @Get('/meta-info')
    async getUserName(@Res() res, @Request() req) {
        const posts = await this.categoryService.getCategoryOwner(req);
        return res.status(HttpStatus.OK).json(posts);
    }

    @Post('/create')
    async createCategory(@Res() res, @Body() createCategoryDto: CreateCategoryDto, @Request() req) {
        const token = getTokenFromHeader(req);
        await this.categoryService.createCategory(createCategoryDto, token);
        return res.status(HttpStatus.OK).json({
            message: 'Категория успешно добавлена!',
        });
    }
}
