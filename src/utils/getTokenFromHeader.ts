export const getTokenFromHeader = (req) => {
    return req.get('authorization').split(' ')[1];
};
