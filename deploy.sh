#!/bin/bash

docker pull registry.gitlab.com/skaarjz/e-note-nest:develop

IS_RUNNING=`docker ps | grep enote-back`

if [[ "$IS_RUNNING" != "" ]]; then
    echo "Deleting old container"
    docker stop enote-back
    docker rm enote-back
fi

echo "Running new container"

docker run -d --name enote-back -p 80:80 --restart="unless-stopped" registry.gitlab.com/skaarjz/e-note-nest:develop
